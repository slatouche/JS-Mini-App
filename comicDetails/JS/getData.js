function GetData()
{
    var inURL = "https://gateway.marvel.com:443/v1/public/comics/"+GetID()+"?apikey=b2c0be59ae77856a6784066abb0ada69";
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() 
    {
        if (this.readyState == 4 && this.status == 200) 
        {
            var charData = JSON.parse(this.responseText);
            SetPageData(charData);
        }
    };
    xhttp.open("GET", inURL, true);
    xhttp.send();
}

function GetID()
{
    return getURLParameter("id").replace("comicID", "");
}