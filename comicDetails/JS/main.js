docReady(function() 
{
    //r$.settings().debug = false;
    //r$.settings().indent = true;
    r$.settings().useMediaQuery = true;
    r$.breakpoints(300, 767, 1280, 1600);
    //r$.breakpoints(300,480,800,1200,1600);
    var color = r$.set('background-color').values('#FF351E', '#99FF26', '#59F3FF', '#AB44FF');
    var width = r$.set('width', 'px').values(300, 500, 600, 700);
    var height = r$.set('height', 'px').values(400, 600, 700, 750);
    var cWidth = r$.set('width', 'px').values(300, 400, 500, 600);
    var cHeight = r$.set('height', 'px').values(400, 500, 600, 700); 
    r$.register('printDatShit',function(w)
    {
        //console.log(w);
    });
    //color.linear().applyTo('.flip-container');
    width.interval().applyTo('.cardWindow');
    height.interval().applyTo('.cardWindow');
    width.interval().applyTo('.detailsWindow');
    height.interval().applyTo('.detailsWindow');
    cWidth.interval().applyTo('.flip-container ');
    cHeight.interval().applyTo('.flip-container ');
    r$.start();
    
   GetData();
});

function SetPageData(inCharData)
{
    var comicData = inCharData["data"]["results"][0];
    
    document.getElementById("comicTitle").textContent = comicData.title;
    document.getElementById("cardWindow").style.backgroundImage = "url('"+GetThumbnailURL(comicData.thumbnail, 0)+"')";
    document.getElementById("descText").textContent = comicData.description || "";
    document.getElementById("creatorsDiv").innerHTML += GetCreators(comicData["creators"]["items"]);
}

function GetCreators(inCreatorsData)
{
    var outText = "";
    for(i = 0; i < inCreatorsData.length; i++)
    {
        outText += '<strong>'+inCreatorsData[i].role+':</strong> '+inCreatorsData[i].name+'<br>';
    }
    return outText;
}