function CardClicked(inCard)
{
    if(!ContainsClass("flip-onHover", inCard.classList))
    {
        inCard.querySelector('.flipper').classList.toggle('flipMe');
    }
}

var letGo = false;
//var prevClass = "";
function TouchStart(inCard)
{
    letGo = false;
    //prevClass = RemoveClass("flip-container", inCard.className);
    inCard.className = "flip-container";
    setTimeout(function () 
    {
        if(!letGo)
        {
            inCard.querySelector('.flipper').className = "flipper flipMe";
        }
    }, 300);
}

function TouchEnd(inCard)
{
    letGo = true;
    inCard.querySelector('.flipper').className = "flipper";
    //inCard.className = "flip-container" + prevClass;
}

function printCssToHtml() 
{
    //console.log(document.getElementById("staticMediaQueries") ? document.getElementById("staticMediaQueries").innerHTML == '');
    var out = document.getElementById("staticMediaQueries");
    //out = String(out).replace(/\n/g, '<br/>').replace(/\t/g, '&nbsp;&nbsp;&nbsp;');
    //document.getElementById("output").innerHTML = out;
    console.log(out.sheet.cssRules[0].cssText);
}

function componentToHex(c) 
{
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

function getColor(w) 
{
    var rgb = w % 255;
    return "#" + componentToHex(rgb) + componentToHex(rgb)+componentToHex(rgb);
}