docReady(function() 
{
    AddCards();
});

function AddCards()
{
    if(pageType == "char")
    {
        for(i = 0; i < pageLimit; i++)
        {
            document.getElementById("cardListWindow").innerHTML += '<div id="charID'+i+'" onclick="CardClicked(this);" class="flip-container"><div class="flipper"><div class="front"><div style="background-Image:url(\'/TestForJS/imgs/placeHolders/cardImg2.jpg\');" class="cardImg"></div><div class="cardName">    <h3 style="color:white">Loading</h3></div></div><div class="back">    <div class="backName">    <h3 style="color:white">Loading</h3></div><div class="statsBar">    <span style="font-size:20px">Stats:</span>    <div class="statBar">        <div class="statBarIcon">            <img src="imgs/statsIcons/power.png"></img>        </div>        <div class="statBarBar">            <div class="meter red nostripes">                            <span style="width: '+RandomIntFromInterval(20,100)+'%"></span>                        </div>                    </div>                </div>                <div class="statBar">        <div class="statBarIcon">            <img src="imgs/statsIcons/toughness.png"></img>        </div>        <div class="statBarBar">            <div class="meter blue nostripes">                            <span style="width: '+RandomIntFromInterval(20,100)+'%"></span>                        </div>                    </div>                </div>                <div class="statBar">        <div class="statBarIcon">            <img src="imgs/statsIcons/magic.png"></img>        </div>        <div class="statBarBar">            <div class="meter purple nostripes">                            <span style="width: '+RandomIntFromInterval(20,100)+'%"></span>                        </div>                    </div>                </div>                <div class="statBar">        <div class="statBarIcon">            <img src="imgs/statsIcons/speed.png"></img>        </div>        <div class="statBarBar">            <div class="meter nostripes">                            <span style="width: '+RandomIntFromInterval(20,100)+'%"></span>                        </div>                    </div>                </div>                <div class="statBar">        <div class="statBarIcon">            <img src="imgs/statsIcons/intelligence.png"></img>        </div>        <div class="statBarBar">            <div class="meter orange nostripes">                            <span style="width: '+RandomIntFromInterval(20,100)+'%"></span>                        </div>                    </div>                </div>            </div>            <div class="detailsBtn">    <button onclick="window.location.href=\'charDetails.html?id=\'+this.parentElement.parentElement.parentElement.parentElement.id" class="btn btn-primary btn-lg sharp">More Details</button>    </div></div></div></div>';
        }
        var apiURL = "https://gateway.marvel.com:443/v1/public/characters?" + GetFilters() + "limit="+ pageLimit +"&offset="+ pageOffset +"&apikey=b2c0be59ae77856a6784066abb0ada69";
        GetCardData(apiURL, pageType);
    }
    else if(pageType == "comic")
    {
        for(i = 0; i < pageLimit; i++)
        {
            document.getElementById("cardListWindow").innerHTML += '<div id="comidID'+i+'" onclick="CardClicked(this);" class="flip-container"><div class="flipper"><div class="front"><div style="background-Image:url(\'/TestForJS/imgs/placeHolders/cardImg2.jpg\');" class="frontCardImg"></div></div><div class="back"><div class="backTitle"></div><div class="backDesc"></div><div class="detailsBtn"><button onclick="window.location.href=\'comicDetails.html?id=\'+this.parentElement.parentElement.parentElement.parentElement.id" class="btn btn-primary btn-lg sharp">More Details</button></div></div></div></div>';
        }
        var apiURL = "https://gateway.marvel.com:443/v1/public/comics?" + GetFilters() + "limit="+ pageLimit +"&offset="+ pageOffset +"&apikey=b2c0be59ae77856a6784066abb0ada69";
        GetCardData(apiURL, pageType);
    }
}

function SetCharCardsData(inData)
{
    //console.log(inData);
    var cards = document.getElementsByClassName("flip-container");
    for(i = 0; i < cards.length; i++)
    {
        if(i < inData["data"]["results"].length)
        {
            var imgURL = GetThumbnailURL(inData["data"]["results"][i]["thumbnail"],imgQuality);
            cards[i].querySelector(".cardImg").style.backgroundImage = "url(" + imgURL + ")";
            cards[i].querySelector(".cardName").innerHTML = "<h2>" + inData["data"]["results"][i].name + "</h2>";
            cards[i].querySelector(".backName").innerHTML = "<h2>" + inData["data"]["results"][i].name + "</h2>";
            cards[i].id = "charID" + inData["data"]["results"][i].id;
        }
        else
        {
            cards[i].style.visibility = "hidden";
        }
    }
}

function SetComicCardsData(inData)
{
    console.log(inData);
    var cards = document.getElementsByClassName("flip-container");
    for(i = 0; i < cards.length; i++)
    {
        if(i < inData["data"]["results"].length)
        {
            var imgURL = GetThumbnailURL(inData["data"]["results"][i]["thumbnail"],imgQuality);
            cards[i].querySelector(".frontCardImg").style.backgroundImage = "url(" + imgURL + ")";
            cards[i].querySelector(".backTitle").innerHTML = "<h2>" + inData["data"]["results"][i].title + "</h2>";
            cards[i].querySelector(".backDesc").innerHTML = "<p>" + inData["data"]["results"][i].description + "</p>";
            cards[i].id = "comicID" + inData["data"]["results"][i].id;
        }
        else
        {
            cards[i].style.visibility = "hidden";
        }
    }
}