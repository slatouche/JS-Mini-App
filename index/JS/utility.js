function ContainsClass(inClassName, inClassList)
{
    var containsClass = false;
    for (var i = 0; i < inClassList.length; i++)
    {
        if(inClassList[i] == inClassName)
        {
            containsClass = true;
            break;
        }
    }
    return containsClass;
}

function RemoveClass(inString, inClassName)
{
    var temp = inClassName.replace(inString,"");
    return temp;
}

function RandomIntFromInterval(min,max)
{
    return Math.floor(Math.random()*(max-min+1)+min);
}

function getURLParameter(inParam) 
{
    return decodeURIComponent((new RegExp('[?|&]' + inParam + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
}

function GetThumbnailURL(inThumbnailData, inSize, inIsHTTPS)
{
    inSize = inSize || 0;
    inIsHTTPS = inIsHTTPS || true;
    var imgSize = "/portrait_";
    if(inSize == 1)
    {
        imgSize += "small.";
    }
    else if(inSize == 2)
    {
        imgSize += "medium.";
    }
    else if(inSize == 3)
    {
        imgSize += "xlarge.";
    }
    else if(inSize == 4)
    {
        imgSize += "fantastic.";
    }
    else if(inSize == 5)
    {
        imgSize += "uncanny.";
    }
    else
    {
        imgSize += "incredible.";
    }
    var thumbnailPath = inThumbnailData["path"];
    if(inIsHTTPS)
    {
        thumbnailPath = thumbnailPath.replace("http://", "https://");
    }
    var thumbnailImgType = inThumbnailData["extension"];
    var thumbnailURL = thumbnailPath + imgSize + thumbnailImgType;
    return thumbnailURL;
}

function GetFilters()
{
    if(pageType == "char")
    {
        if(filterType != "")
        {
            if(filterType == "char")
            {
                return "nameStartsWith=" + filterValue + "&";
            }
            else
            {
                return "";
            }
        }
        else
        {
            return "";
        }
    }
    else
    {
        if(filterType != "")
        {
            if(filterType == "char")
            {
                //return "nameStartsWith=" + filterValue + "&";
                //Do call to get list of char id's
                var tempCharData = JSON.parse(GetData("https://gateway.marvel.com:443/v1/public/characters?nameStartsWith="+filterValue+"&limit=1&offset=0&apikey=b2c0be59ae77856a6784066abb0ada69"));
                var tempCharIDs = "";
                for(i=0;i<tempCharData["data"]["results"].length;i++)
                {
                    tempCharIDs += tempCharData["data"]["results"][i].id;
                    if(i < (tempCharData["data"]["results"].length-1))
                    {
                        tempCharIDs += "%2C";
                    }
                }
                //then return
                if(tempCharIDs == "")
                {
                    tempCharIDs = "1010727";
                }
                    return "characters=" + tempCharIDs + "&";
            }
            else
            {
                return "titleStartsWith=" + filterValue + "&";
            }
        }
        else
        {
            return "";
        }
    }
}