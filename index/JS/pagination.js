//Called From getData as totalNum of results is needed
function MakeTablePagination(inTotalNum, inNumButtonsOnSide)
{
    inNumButtonsOnSide = inNumButtonsOnSide || 0;
    if(inNumButtonsOnSide < 0)
    {
        inNumButtonsOnSide = 0;
    }
    var highestPageNum = parseInt((inTotalNum/pageLimit)+1);
    if(pageNum > 0)
    {
        //Vars
        var numOnLeftSide = 0;
        var numOnRightSide = 0;
        
        //If we need to put buttons on side
        if(inNumButtonsOnSide > 0)
        {
            //calculate num of pageNums to show on left side
            numOnLeftSide = inNumButtonsOnSide;
            if((pageNum - inNumButtonsOnSide) < 1)
            {
                numOnLeftSide = (inNumButtonsOnSide + (pageNum - inNumButtonsOnSide)) - 1;
            }
            //calculate num of pageNums to show on right side
            numOnRightSide = inNumButtonsOnSide;
            if((pageNum + inNumButtonsOnSide) > highestPageNum)
            {
                numOnRightSide = inNumButtonsOnSide + (highestPageNum-(pageNum + inNumButtonsOnSide));
            }
            //Now add to other side to make it have correct total
            if((inNumButtonsOnSide - numOnLeftSide) > 0)
            {
                numOnRightSide += (inNumButtonsOnSide - numOnLeftSide);
            }
            else
            {
                numOnLeftSide += (inNumButtonsOnSide - numOnRightSide);
            }
        }
        
        //Now Start Making Buttons
        var pagination = document.getElementById("myPagination");
        
        //Prev/First Button
        if(pageNum > 1)
        {
            pagination.innerHTML += "<a href=\"" + GetChangePageURL(pageNum-1) + "\">&laquo;</a>";
            //pagination.innerHTML += "<a href=\"" + GetChangePageURL(1) + "\">&laquo;</a>";
        }
        
        //Left Side Buttons
        for(i = numOnLeftSide; i >= 1; i --)
        {
            pagination.innerHTML += MakePageButton((pageNum-i),pageNum);
        }
        
        //Current Page Button
        pagination.innerHTML += MakePageButton(pageNum,pageNum);
        
        //Right Side Buttons
        for(i = 1; i <= numOnRightSide; i ++)
        {
            pagination.innerHTML += MakePageButton((pageNum+i),pageNum);
        }
        
        //Next/Last Button
        if(pageNum < highestPageNum)
        {
            //pagination.innerHTML += "<a href=\"" + GetChangePageURL(highestPageNum) + "\">&raquo;</a>";
            pagination.innerHTML += "<a href=\"" + GetChangePageURL(pageNum+1) + "\">&raquo;</a>";
        }
    }
    else
    {
        console.log("Problem With Page Number");
    }
}

function MakePageButton(inCurNum, inPageNum)
{
    var outHTML = "";
    if(inCurNum == inPageNum)
    {
        outHTML += "<a href=\"" + GetChangePageURL(inCurNum) + "\" class=\"active\">" + inCurNum + "</a>";
    }
    else
    {
        outHTML += "<a href=\"" + GetChangePageURL(inCurNum) + "\">" + inCurNum + "</a>";
    }
    return outHTML;
}

function GetChangePageURL(inPageNum)
{
    if(location.search)
    {
        var tempSplitUrl = location.search.split(/pN=(.+)/);
        var removedPageNum = tempSplitUrl[1].split(/&(.+)/);
        var outPageURL = tempSplitUrl[0] + "pN=" + inPageNum;
        if(removedPageNum[1])
        {
            outPageURL += "&" + removedPageNum[1]
        }
        return outPageURL;
    }
    else
    {
        return ("?pT=char&pN=" + inPageNum);
    }
}