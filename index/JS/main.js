var pageType = getURLParameter("pT") || "char";
var pageLimit = parseInt(getURLParameter("pL")) || 20;
var pageNum = parseInt(getURLParameter("pN")) || 1;
var pageOffset = ( (pageNum-1) * pageLimit );
var cardsData = "";
var filterType = getURLParameter("fT") || "";
var filterValue = getURLParameter("fV") || "";
var numOfSidePageButtons = 0;
var imgQuality = 4;

docReady(function() 
{
    if(getURLParameter("pT") == "comic")
    {
        document.getElementById("charListNav").style.backgroundImage = "url('/TestForJS/imgs/buttons/charNotSelected.jpg')";
        document.getElementById("comicListNav").style.backgroundImage = "url('/TestForJS/imgs/buttons/comicSelected.jpg')";
        document.getElementById("headerBackSearchBtnArea").onclick = function(){SearchComicFilter()};
        document.getElementById("hideSeriesInput").style.visibility = "visible";
        document.getElementById("hideCharInput").style.visibility = "hidden";
        document.getElementById("seriesFilter").checked = true;
        document.getElementById("filterText").focus;
        //console.log(document.getElementById("hideSeriesInput").style.visibility);
    }
    
    //r$.settings().debug = false;
    //r$.settings().indent = true;
    r$.settings().useMediaQuery = true;
    r$.breakpoints(300, 767, 1280, 1600);
    //r$.breakpoints(300,480,800,1200,1600);
    var color = r$.set('background-color').values('#FF351E', '#99FF26', '#59F3FF', '#AB44FF');
    var width = r$.set('width', 'px').values(280, 300, 300, 300);
    var height = r$.set('height', 'px').values(400, 450, 450, 450);
    r$.register('printDatShit',function(w)
    {
        //console.log(w);
    });
    //color.linear().applyTo('.flip-container');
    width.interval().applyTo('.flip-container');
    height.interval().applyTo('.flip-container');
    r$.start();
});

function FlipHeader()
{
    document.getElementById("header").querySelector('.hFlipper').classList.toggle('hFlipMe');
    document.getElementById("filterText").focus();
}

function SearchCharFilter()
{
    var temFilterValue = document.getElementById("filterText").value || "";
    var hrefLoc = "?pT=" + pageType + "&pN=1";
    if(temFilterValue != "")
    {
       hrefLoc += "&fT=char&fV=" + temFilterValue;
    }
    window.location.href = hrefLoc;
}

function SearchComicFilter()
{
    var temFilterValue = document.getElementById("filterText").value || "";
    var hrefLoc = "?pT=" + pageType + "&pN=1";
    if(temFilterValue != "")
    {
        var tempFilterType = "char";
        if(document.getElementById("seriesFilter").checked)
        {
            tempFilterType = "series";
        }
        hrefLoc += "&fT="+tempFilterType+"&fV=" + temFilterValue;
    }
    window.location.href = hrefLoc;
}