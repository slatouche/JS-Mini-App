function GetCardData(inURL, inPageType)
{
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() 
    {
        if (this.readyState == 4 && this.status == 200) 
        {
            cardsData = JSON.parse(this.responseText);
            if(inPageType == "char")
            {
                SetCharCardsData(cardsData);
            }
            else
            {
                SetComicCardsData(cardsData);
            }
            MakeTablePagination(cardsData["data"].total, numOfSidePageButtons);
        }
    };
    xhttp.open("GET", inURL, true);
    xhttp.send();
}

function GetData(inURL)
{
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", inURL, false);
    xhttp.send();
    return xhttp.responseText;
}

function LoadFromFile(inURL)
{
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() 
    {
        if (this.readyState == 4 && this.status == 200) 
        {
            console.log(this.responseText);
        }
    };
    xhttp.open("GET", inURL, false);
    xhttp.send();
}